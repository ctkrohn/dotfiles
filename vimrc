set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'L9'
Plugin 'CSApprox'
Plugin 'Syntastic'
Plugin 'The-NERD-tree'
Plugin 'The-NERD-Commenter'
Bundle('https://bitbucket.org/ctkrohn/ctkvimfiles.git')

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"
:syntax enable
:colorscheme ctkrohn

" New lines are indented to level of previous line
:set autoindent

" Operating system-specific options
if has("win32")
	" Set a nice font
	:set guifont=Consolas:h10:cANSI
	" Make Vim clipboard match Windows one
	:set clipboard=unnamed
elseif has("unix")
	" Nice colors in terminals
	:set t_Co=256
	" Make Vim clipboard match Linux one
	:set clipboard=unnamedplus
endif

" Better tab commands
:map <C-n> :tabn<CR>
:map <C-p> :tabp<CR>

" Remove unnecessary GUI cruft
:set guioptions-=m
:set guioptions-=e
:set guioptions-=T
:set guioptions-=r
:set guioptions-=L

" Full path names in tab title
:set guitablabel=%f

" Line numbers on left
:set nu

" Remap window movement commands
:map <C-h> <C-w>h
:map <C-j> <C-w>j
:map <C-k> <C-w>k
:map <C-l> <C-w>l

" Disable automatic comments
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Always display a status line
:set laststatus=2
:set statusline=%F%m%r%h%w\ %v,%l/%L\ %P " And make it somewhat informative

" Highlight text beyond 80 characters wide
:set colorcolumn=80

" Set reasonable backspace options
:set backspace=indent,eol,start

" Highlight current line
:set cursorline!

" Disable annoying dings
:set vb t_vb=

" Automatically resize splits when window is resized
au VimResized * exe "normal! \<c-w>="

" Searching options
set hlsearch
nnoremap <esc> :noh<return><esc>
set incsearch
set ignorecase
set smartcase

" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 0
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_python_exec='/usr/bin/python3'
let g:syntastic_python_checkers = ['flake8']
